/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/callback.h>
#include <caml/bigarray.h>

#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "pluginclass_stub.h"
#include "node_stub.h"

value ocaml_lilv_alloc_plugin_class_array(value _world, const LilvPluginClasses* plugin_classes)
{
  CAMLparam1(_world);
  CAMLlocal1(ans);

  unsigned plugin_classes_size = lilv_plugin_classes_size(plugin_classes);

  ans = caml_alloc_tuple(plugin_classes_size);

  int i = 0;
  LILV_FOREACH(plugin_classes, iter, plugin_classes) {

    const LilvPluginClass* plugin_class = lilv_plugin_classes_get(plugin_classes, iter);

    Store_field(ans, i, ocaml_lilv_alloc_plugin_class(_world, plugin_class));
    i++;
  }

  CAMLreturn(ans);
}

CAMLprim value ocaml_lilv_plugin_class_get_parent_uri(value _plugin_class)
{
  CAMLparam1 (_plugin_class);
  const LilvNode* n = lilv_plugin_class_get_parent_uri(PluginClass_val(_plugin_class));
  CAMLreturn (ocaml_lilv_node_to_string(n));
}

//LilvNode*lilv_plugin_class_get_uri(LilvPluginClass plugin_class);
CAMLprim value ocaml_lilv_plugin_class_get_uri(value _plugin_class)
{
  CAMLparam1 (_plugin_class);
  const LilvNode* n = lilv_plugin_class_get_uri(PluginClass_val(_plugin_class));
  CAMLreturn (ocaml_lilv_node_to_string(n));
}

//LilvNode*lilv_plugin_class_get_label(LilvPluginClass plugin_class);
CAMLprim value ocaml_lilv_plugin_class_get_label(value _plugin_class)
{
  CAMLparam1 (_plugin_class);
  const LilvNode* n = lilv_plugin_class_get_label(PluginClass_val(_plugin_class));
  CAMLreturn (ocaml_lilv_node_to_string(n));
}

//LilvPluginClasses* lilv_plugin_class_get_children(LilvPluginClass plugin_class);
CAMLprim value ocaml_lilv_plugin_class_get_children(value _plugin_class)
{
  CAMLparam1 (_plugin_class);
  CAMLlocal1 (ans);
  LilvPluginClasses* pcs = lilv_plugin_class_get_children(PluginClass_val(_plugin_class));
  ans = ocaml_lilv_alloc_plugin_class_array(Owner_val(_plugin_class), pcs);
  lilv_plugin_classes_free(pcs);
  CAMLreturn (ans);
}
