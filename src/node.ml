
type t


external equals : t -> t -> bool = "ocaml_lilv_node_equals"

external is_uri : t -> bool = "ocaml_lilv_node_is_uri"

external is_literal : t -> bool = "ocaml_lilv_value_is_literal"

external is_string : t -> bool = "ocaml_lilv_node_is_string"

external as_string : t -> string = "ocaml_lilv_node_as_string"

external is_float : t -> bool = "ocaml_lilv_node_is_float"

external as_float : t -> float = "ocaml_lilv_node_as_float"

external is_int : t -> bool = "ocaml_lilv_node_is_int"

external as_int : t -> int = "ocaml_lilv_node_as_int"


