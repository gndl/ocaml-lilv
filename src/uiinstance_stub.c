/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/fail.h>

#include <stdbool.h>
#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "lv2_stub.h"
#include "node_stub.h"
#include "plugin_stub.h"
#include "ui_stub.h"
#include "uiinstance_stub.h"

static void caml_ui_instance_finalize (value v_ui_instance)
{
	lilv_ui_instance_free(UIInstance_val(v_ui_instance));
}

// Encapsulation of opaque ui instance handles (of type LilvUIInstance) as Caml custom blocks.
static struct custom_operations lilv_ui_instance_ops = {
  "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/ui_instance",
  caml_ui_instance_finalize,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

/* Allocating a Caml custom block to hold the given LilvUIInstance */
value caml_alloc_ui_instance(LilvUIInstance ui_instance)
{
  value r = caml_alloc_custom(&lilv_ui_instance_ops, sizeof(LilvUIInstance), sizeof(LilvUIInstance), sizeof(LilvUIInstance));
  UIInstance_val(r) = ui_instance;
  return r;
}

void lilv_ui_write_function(LV2UI_Controller controller, uint32_t port_index,
		uint32_t buffer_size, uint32_t format, const void* buffer)
{
	//TODO
}

value caml_lilv_ui_instantiate(value v_plugin, value v_ui,
		value v_write_function, value v_controller, value v_features)
{
   CAMLparam5 (v_plugin, v_ui, v_write_function, v_controller, v_features);
   LilvUIInstance uiinst = lilv_ui_instantiate(Plugin_val(v_plugin),
		   UI_val(v_ui), lilv_ui_write_function,
		   Data_custom_val(v_controller), Data_custom_val(v_features));

   if(uiinst == NULL) caml_failwith("Lilv.UI instantiation failed");

   CAMLreturn (caml_alloc_ui_instance(uiinst));
}


/** Free a plugin UI instance.
 *
 * It is the caller's responsibility to ensure all references to the UI
 * instance (including any returned widgets) are cut before calling
 * this function.
 *
 * \a instance is invalid after this call.
 */
//void lilv_ui_instance_free(LilvUIInstance instance);
value caml_lilv_ui_instance_free(value v_ui_instance)
{
   CAMLparam1 (v_ui_instance);
   lilv_ui_instance_free(UIInstance_val(v_ui_instance));
   CAMLreturn (Val_unit);
}


/** Get the widget for the UI instance.
 */
//LV2UI_Widget lilv_ui_instance_get_widget(LilvUIInstance instance);
value caml_lilv_ui_instance_get_widget(value v_ui_instance)
{
   CAMLparam1 (v_ui_instance);
   LV2UI_Widget w = lilv_ui_instance_get_widget(UIInstance_val(v_ui_instance));
   CAMLreturn (caml_alloc_ui_widget(v_ui_instance, w));
}


/** Get the LV2UI_Descriptor of the plugin UI instance.
 *
 * Normally hosts should not need to access the LV2UI_Descriptor directly,
 * use the lilv_ui_instance_* functions.
 *
 * The returned descriptor is shared and must not be deleted.
 *//*
const LV2UI_Descriptor*
lilv_ui_instance_get_descriptor(LilvUIInstance instance);
*/

/** Get the LV2UI_Handle of the plugin UI instance.
 *
 * Normally hosts should not need to access the LV2UI_Handle directly,
 * use the lilv_ui_instance_* functions.
 * 
 * The returned handle is shared and must not be deleted.
 *//*
LV2UI_Handle
lilv_ui_instance_get_handle(LilvUIInstance instance);
*/
