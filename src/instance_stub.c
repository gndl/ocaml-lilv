/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/bigarray.h>
#include <caml/threads.h>

#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "lv2_stub.h"
#include "feature_stub.h"
#include "plugin_stub.h"
#include "instance_stub.h"

static void custom_finalize_instance (value _instance)
{
  lilv_instance_free(Instance_val(_instance));
}

// Encapsulation of opaque plugin instance handles (of type LilvInstance) as Caml custom blocks.
static struct custom_operations lilv_instance_ops =
  {
   "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/instance",
   custom_finalize_instance,
   custom_compare_default,
   custom_hash_default,
   custom_serialize_default,
   custom_deserialize_default
  };

/* Allocating a Caml custom block to hold the given LilvInstance */
static void ocaml_lilv_alloc_instance(LilvInstance* instance, value * ans)
{
  *ans = caml_alloc_custom(&lilv_instance_ops, sizeof(LilvInstance*), 0, 1);
  Instance_val(*ans) = instance;
}

//LilvInstance lilv_plugin_instantiate(LilvPlugin plugin, double sample_rate, const LV2_Feature*const* features);
CAMLprim value ocaml_lilv_plugin_instantiate(value _plugin, value _sample_rate, value _features)
{
  CAMLparam3 (_plugin, _sample_rate, _features);
  CAMLlocal1(ans);
  LilvPlugin * plugin = Plugin_val(_plugin);
  double sample_rate = Double_val(_sample_rate);
  int features_count = Wosize_val(_features);
  const LV2_Feature** features = calloc(features_count + 1, sizeof(LV2_Feature*));

  if(features == NULL) caml_failwith("instantiation failed");

  for (int i = 0; i < features_count; i++) {
    features[i] = Lv2Feature_val(Field(_features, i));
  }

  caml_release_runtime_system();
  LilvInstance* inst = lilv_plugin_instantiate(plugin, sample_rate, features);

  free(features);
  caml_acquire_runtime_system();

  if(inst == NULL) caml_failwith("instantiation failed");

  ocaml_lilv_alloc_instance(inst, &ans);
  
  CAMLreturn (ans);
}

//static inline const char* lilv_instance_get_uri(LilvInstance* instance)
CAMLprim value ocaml_lilv_instance_get_uri(value _instance)
{
  CAMLparam1 (_instance);
  const char* uri = lilv_instance_get_uri(Instance_param(_instance));
  CAMLreturn (caml_copy_string(uri));
}

//static inline void lilv_instance_connect_port(LilvInstance* instance, uint32_t port_index, void*data_location)
value ocaml_lilv_instance_connect_floats_port(value _instance, value _port_index, value _data)
{
  lilv_instance_connect_port(Instance_param(_instance),
                             (uint32_t)Int_val(_port_index), Caml_ba_data_val(_data));
  return Val_unit;
}

//static inline void lilv_instance_activate(LilvInstance* instance)
value ocaml_lilv_instance_activate(value _instance)
{
  lilv_instance_activate(Instance_param(_instance));
  return Val_unit;
}

//static inline void lilv_instance_run(LilvInstance* instance, uint32_t sample_count)
value ocaml_lilv_instance_run(value _instance, value _sample_count)
{
  lilv_instance_run(Instance_param(_instance), (uint32_t)Int_val(_sample_count));
  return Val_unit;
}

//static inline void lilv_instance_deactivate(LilvInstance* instance)
value ocaml_lilv_instance_deactivate(value _instance)
{
  lilv_instance_deactivate(Instance_param(_instance));
  return Val_unit;
}

// const void* lilv_instance_get_extension_data(LilvInstance* instance, const char*  uri)
/*
  value caml_lilv_instance_get_extension_data(value _instance, value _uri)
  {
  CAMLparam2 (_instance, _uri);
  const void* data = lilv_instance_get_extension_data(Instance_val(_instance), String_val(_uri));
  CAMLreturn (caml_alloc_data(data));
  }
*/
