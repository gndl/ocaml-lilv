
type floats_t = (float, Bigarray.float32_elt, Bigarray.c_layout) Bigarray.Array1.t

type float_t = floats_t

type t = [
  | `AtomData of floats_t
  | `AudioData of floats_t
  | `ControlData of float_t
  | `CVData of floats_t
  | `EventData of floats_t
]
