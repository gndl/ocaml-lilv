type stub
type t = {stub : stub; features : Feature.t list}


let make stub features = {stub; features}


external get_uri : t -> string = "ocaml_lilv_instance_get_uri"


external connect_float_port : t -> int -> Data.float_t -> unit = "ocaml_lilv_instance_connect_floats_port"
external connect_floats_port : t -> int -> Data.floats_t -> unit = "ocaml_lilv_instance_connect_floats_port"

let connect_port instance port_index = function
  | `AtomData data -> connect_floats_port instance port_index data
  | `AudioData data -> connect_floats_port instance port_index data
  | `ControlData data -> connect_float_port instance port_index data
  | `CVData data -> connect_floats_port instance port_index data
  | `EventData data -> connect_floats_port instance port_index data


external activate : t -> unit = "ocaml_lilv_instance_activate"

external run : t -> int -> unit = "ocaml_lilv_instance_run"

external deactivate : t -> unit = "ocaml_lilv_instance_deactivate"


