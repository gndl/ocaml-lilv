type t

external _load_all : unit -> t = "ocaml_lilv_world_load_all"

let load_all() =
  try
    Ok(_load_all())
  with Failure msg -> Error(`Lilv_world_initialization_failure msg)


external get_plugin_class : t -> Pluginclass.t = "ocaml_lilv_world_get_plugin_class"
external get_plugin_classes : t -> Pluginclass.t array = "ocaml_lilv_world_get_plugin_classes"
external get_all_plugins : t -> Plugin.t array = "ocaml_lilv_world_get_all_plugins"


