type t

external get_parent_uri : t -> string = "ocaml_lilv_plugin_class_get_parent_uri"

external get_uri : t -> string = "ocaml_lilv_plugin_class_get_uri"

external get_label : t -> string = "ocaml_lilv_plugin_class_get_label"

external get_children : t -> t array = "ocaml_lilv_plugin_class_get_children"



