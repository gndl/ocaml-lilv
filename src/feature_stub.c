/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <string.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/callback.h>
//#include <caml/bigarray.h>
#include <caml/threads.h>

#include "lv2/lv2plug.in/ns/ext/log/log.h"

#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "lv2_stub.h"
#include "feature_stub.h"

static void custom_finalize_feature (value _feature)
{
  OcamlLv2Feature * feature = Feature_val(_feature);

  if(feature->associated_value != (value)NULL) {
    caml_remove_generational_global_root(&feature->associated_value);
  }

  free(feature);
}

static struct custom_operations lilv_feature_ops =
  {
   "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/feature",
   custom_finalize_feature,
   custom_compare_default,
   custom_hash_default,
   custom_serialize_default,
   custom_deserialize_default
  };

static OcamlLv2Feature * ocaml_lilv_alloc_feature(const char * uri, size_t data_size, value associated_value, value * ans)
{
  OcamlLv2Feature * feature = (OcamlLv2Feature*)calloc(1, sizeof(OcamlLv2Feature) + data_size);
  if(!feature) caml_failwith("Lilv.Feature instantiation failed");

  feature->lv2.URI = uri;

  if(data_size > 0) {
    feature->lv2.data = (void*)(((char*)feature) + sizeof(OcamlLv2Feature));
  }

  if(associated_value != (value)NULL) {
    feature->associated_value = associated_value;
    caml_register_generational_global_root(&feature->associated_value);
  }

  *ans = caml_alloc_custom(&lilv_feature_ops, sizeof(OcamlLv2Feature*), 0, 1);
  Feature_val(*ans) = feature;

  return feature;
}

CAMLprim value ocaml_lilv_feature_lv2(value _id)
{
  CAMLparam0 ();
  CAMLlocal1(ans);

  ocaml_lilv_alloc_feature(ocaml_lilv_lv2_uri(Int_val(_id)), 0, (value)NULL, &ans);
  
  CAMLreturn (ans);
}


/**
   Log feature (LV2_LOG__log)
*/
#define LINE_SIZE 1024

static int ocaml_lilv_feature_log_vprintf(LV2_Log_Handle handle, LV2_URID type, const char* fmt, va_list vl)
{
  OcamlLv2Feature * feature = (OcamlLv2Feature*)handle;
  char line[LINE_SIZE + 1];

  int ret = vsnprintf(line, LINE_SIZE, fmt, vl);

  ocaml_lilv_register_thread();
  caml_acquire_runtime_system();
  caml_callback2(feature->associated_value, Val_long(type), caml_copy_string(line));
  caml_release_runtime_system();

  return ret;
}

static int ocaml_lilv_feature_log_printf(LV2_Log_Handle handle, LV2_URID type, const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  const int ret = ocaml_lilv_feature_log_vprintf(handle, type, fmt, args);
  va_end(args);
  return ret;
}

CAMLprim value ocaml_lilv_feature_log(value _cb)
{
  CAMLparam1(_cb);
  CAMLlocal1(ans);

  OcamlLv2Feature * feature = ocaml_lilv_alloc_feature(LV2_LOG__log, sizeof(LV2_Log_Log), _cb, &ans);
  LV2_Log_Log * data = (LV2_Log_Log*)feature->lv2.data;

  data->handle = feature;
  data->printf = ocaml_lilv_feature_log_printf;
  data->vprintf = ocaml_lilv_feature_log_vprintf;

  CAMLreturn (ans);
}


/**
  URI => Int map feature (LV2_URID__map)
*/
static LV2_URID ocaml_lilv_feature_default_map_uri(LV2_URID_Map_Handle handle, const char* uri)
{
  for(LV2_URID ret = 0; ret < OCAML_LILV_LV2_NODES_LEN; ++ret) {
    if(0 == strcmp(uri, ocaml_lilv_lv2_uri(ret))) return ret;
  }
  return OCAML_LILV_LV2_NODES_LEN;
}

CAMLprim value ocaml_lilv_feature_default_urid_map()
{
  CAMLparam0();
  CAMLlocal1(ans);

  OcamlLv2Feature * feature = ocaml_lilv_alloc_feature(LV2_URID__map, sizeof(LV2_URID_Map), (value)NULL, &ans);
  LV2_URID_Map * data = (LV2_URID_Map*)feature->lv2.data;

  data->handle = feature;
  data->map = ocaml_lilv_feature_default_map_uri;
        
  CAMLreturn (ans);
}

static LV2_URID ocaml_lilv_feature_map_uri(LV2_URID_Map_Handle handle, const char* uri)
{
  CAMLparam0();
  CAMLlocal1(res);
  LV2_URID ret = 0;
  OcamlLv2Feature * feature = (OcamlLv2Feature*)handle;

  ocaml_lilv_register_thread();
  caml_acquire_runtime_system();

  res = caml_callback_exn(feature->associated_value, caml_copy_string(uri));

  if(Is_exception_result(res)) {
    res = Extract_exception(res);
  }
  else {
    ret = (LV2_URID)Long_val(res);
  }

  caml_release_runtime_system();

  CAMLreturnT(LV2_URID, ret);
}

CAMLprim value ocaml_lilv_feature_urid_map(value _cb)
{
  CAMLparam1(_cb);
  CAMLlocal1(ans);

  OcamlLv2Feature * feature = ocaml_lilv_alloc_feature(LV2_URID__map, sizeof(LV2_URID_Map), _cb, &ans);
  LV2_URID_Map * data = (LV2_URID_Map*)feature->lv2.data;

  data->handle = feature;
  data->map = ocaml_lilv_feature_map_uri;
        
  CAMLreturn (ans);
}


/**
  Int => URI map feature (LV2_URID__unmap)
*/
static const char* ocaml_lilv_feature_default_unmap_uri(LV2_URID_Unmap_Handle handle, LV2_URID urid)
{
  if(urid < OCAML_LILV_LV2_NODES_LEN) return ocaml_lilv_lv2_uri(urid);
  return NULL;
}

CAMLprim value ocaml_lilv_feature_default_urid_unmap()
{
  CAMLparam0();
  CAMLlocal1(ans);

  OcamlLv2Feature * feature = ocaml_lilv_alloc_feature(LV2_URID__unmap, sizeof(LV2_URID_Unmap), (value)NULL, &ans);
  LV2_URID_Unmap * data = (LV2_URID_Unmap*)feature->lv2.data;

  data->handle = feature;
  data->unmap = ocaml_lilv_feature_default_unmap_uri;

  CAMLreturn (ans);
}

static const char* ocaml_lilv_feature_unmap_uri(LV2_URID_Unmap_Handle handle, LV2_URID urid)
{
  CAMLparam0();
  CAMLlocal1(res);
  const char* ret = NULL;
  OcamlLv2Feature * feature = (OcamlLv2Feature*)handle;

  ocaml_lilv_register_thread();
  caml_acquire_runtime_system();

  res = caml_callback_exn(feature->associated_value, Val_long(urid));

  if(Is_exception_result(res)) {
    res = Extract_exception(res);
  }
  else {
    //    ret = TODO
  }

  caml_release_runtime_system();

  CAMLreturnT(const char*, ret);
}

CAMLprim value ocaml_lilv_feature_urid_unmap(value _cb)
{
  CAMLparam1(_cb);
  CAMLlocal1(ans);

  OcamlLv2Feature * feature = ocaml_lilv_alloc_feature(LV2_URID__unmap, sizeof(LV2_URID_Unmap), _cb, &ans);
  LV2_URID_Unmap * data = (LV2_URID_Unmap*)feature->lv2.data;

  data->handle = feature;
  data->unmap = ocaml_lilv_feature_unmap_uri;

  CAMLreturn (ans);
}
