/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __OCAML_LILV_H_
#define __OCAML_LILV_H_

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>

#define OCAML_LILV_VERSION "v0.24.0000"

void ocaml_lilv_register_thread();

#define HIGH_POS 0
#define LOW_POS 1
#define OWNER_POS 2

value ocaml_lilv_alloc_owned_data(value owner, const void * data);


#define Owner_val(v) Field(v, OWNER_POS)
#define OwnedData_val(T, v)                                             \
  ((T*)(((Long_val(Field((v), HIGH_POS))) << 8) | ((Long_val(Field((v), LOW_POS))) & 0xFF)))


#endif /*__OCAML_LILV_H_*/
