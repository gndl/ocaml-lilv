/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <pthread.h>

#include "ocaml_lilv.h"

#include <caml/threads.h>


value ocaml_lilv_alloc_owned_data(value _owner, const void * data)
{
  CAMLparam1 (_owner);
  CAMLlocal1 (ans);

  ans = caml_alloc_tuple(3);
  Store_field(ans, HIGH_POS, Val_long(((long)data) >> 8));
  Store_field(ans, LOW_POS, Val_long(((long)data)));
  Store_field(ans, OWNER_POS, (_owner));

  CAMLreturn(ans);
}


/***** C THREADS REGISTRATION *****/

static pthread_key_t ocaml_c_thread_key;
static pthread_once_t ocaml_c_thread_key_once = PTHREAD_ONCE_INIT;

static void ocaml_lilv_on_thread_exit(void *key)
{
  caml_c_thread_unregister();
}

static void ocaml_lilv_make_key()
{
  pthread_key_create(&ocaml_c_thread_key, ocaml_lilv_on_thread_exit);
}

void ocaml_lilv_register_thread()
{
  static int initialized = 1;

  pthread_once(&ocaml_c_thread_key_once, ocaml_lilv_make_key);

  if (NULL == pthread_getspecific(ocaml_c_thread_key)) {
    pthread_setspecific(ocaml_c_thread_key, (void*)&initialized);
    caml_c_thread_register();
  }
}
