/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __OCAML_LILV_PLUGIN_STUB_H_
#define __OCAML_LILV_PLUGIN_STUB_H_

#include <caml/mlvalues.h>
#include "lilv/lilv.h"
#include "ocaml_lilv.h"

/* Accessing the LilvPlugin part of a Caml custom block */
/*
#define Plugin_val(v) (*((LilvPlugin**) Data_custom_val(v)))
#define ConstPlugin_val(v) (*((const LilvPlugin**) Data_custom_val(v)))

#define PLUGIN_PLUGIN_POS 0
#define PLUGIN_WORLD_POS 1

#define Plugin_param(p) Plugin_val(Field(p, PLUGIN_PLUGIN_POS))
#define PluginWorldVal_param(p) Field(p, PLUGIN_WORLD_POS)
*/
#define Plugin_val(v) OwnedData_val(LilvPlugin, v)
#define ConstPlugin_val(v) Plugin_val(v)

value ocaml_lilv_alloc_plugin_array(value world, const LilvPlugins* plugins);

#endif /*__OCAML_LILV_PLUGIN_STUB_H_*/
