/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>

#include <stdbool.h>
#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "world_stub.h"
#include "node_stub.h"

// Encapsulation of opaque value handles (of type LilvNode) as Caml custom blocks.
static struct custom_operations lilv_node_ops =
  {
   "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/node",
   custom_finalize_default,
   custom_compare_default,
   custom_hash_default,
   custom_serialize_default,
   custom_deserialize_default
  };

/* Allocating a Caml custom block to hold the given LilvNode */
value caml_alloc_value(const LilvNode* node)
{
  value v = caml_alloc_custom(&lilv_node_ops, sizeof(LilvNode*), 0, 1);
  ConstNode_val(v) = node;
  return v;
}

void caml_alloc_node(const LilvNode* node, value * pvalue)
{
  *pvalue = caml_alloc_custom(&lilv_node_ops, sizeof(LilvNode*), 0, 1);
  ConstNode_val((*pvalue)) = node;
}

static void custom_finalize_node (value _node)
{
  lilv_node_free(Node_val(_node));
}

static struct custom_operations lilv_gc_node_ops =
  {
   "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/gcnode",
   custom_finalize_node,
   custom_compare_default,
   custom_hash_default,
   custom_serialize_default,
   custom_deserialize_default
  };

/* Allocating a Caml custom block to hold the given LilvNode */
void caml_alloc_gc_node(const LilvNode* node, value * pvalue)
{
  *pvalue = caml_alloc_custom(&lilv_gc_node_ops, sizeof(LilvNode*), 0, 1);
  ConstNode_val((*pvalue)) = node;
}

value ocaml_lilv_alloc_node_to_free(const LilvNode* node)
{
  value v = caml_alloc_custom(&lilv_gc_node_ops, sizeof(LilvNode*), 0, 1);
  ConstNode_val(v) = node;
  return v;
}

void caml_alloc_node_list(const LilvNodes* nodes, bool gc, value * pvalue)
{
  CAMLparam0();
  CAMLlocal4 (firstElem, elem, nextElem, vnode);
  unsigned nodesSize = lilv_nodes_size(nodes);
   
  if(nodesSize > 0) {
    void(*alloc_node)(const LilvNode*, value*) = caml_alloc_node;
    if (gc) alloc_node = caml_alloc_gc_node;

    LilvIter* iter = lilv_nodes_begin(nodes);
    const LilvNode* p = lilv_nodes_get(nodes, iter);

    firstElem = elem = caml_alloc(2, 0);
    alloc_node(p, &vnode);
    Store_field(elem, 0, vnode);

    iter = lilv_nodes_next(nodes, iter);

    while( ! lilv_nodes_is_end(nodes, iter)) {

      p = lilv_nodes_get(nodes, iter);

      nextElem = caml_alloc(2, 0);
      alloc_node(p, &vnode);
      Store_field(nextElem, 0, vnode);
      Store_field(elem, 1, nextElem);

      elem = nextElem;
      iter = lilv_nodes_next(nodes, iter);
    }
    Store_field(elem, 1, Val_int(0));
    /*
      LilvNode* p = lilv_nodes_get_at(nodes, nodesSize - 1);
      firstElem = caml_alloc(2, 0);
	   
      Store_field(firstElem, 0, alloc_node(p));
      Store_field(firstElem, 1, Val_int(0));
	   
      for(--nodesSize; nodesSize > 0; nodesSize--)
      {
      p = lilv_nodes_get_at(nodes, nodesSize - 1);
      nextElem = firstElem;
      firstElem = caml_alloc(2, 0);
      Store_field(firstElem, 0, alloc_node(p));
      Store_field(firstElem, 1, nextElem);
      }
    */
  }
  else {
    firstElem = Val_int(0);
  }
  *pvalue = firstElem;
}

value caml_alloc_value_list(const LilvNodes* nodes, bool gc)
{
  CAMLparam0();
  CAMLlocal1 (ans);
  caml_alloc_node_list(nodes, gc, &ans);
  CAMLreturn (ans);
}

//LilvNode* lilv_node_new_uri(LilvWorld world, const char* uri);
value caml_lilv_node_new_uri(value _world, value _uri)
{
  CAMLparam2 (_world, _uri);
  CAMLlocal1 (ans);
  LilvNode* v = lilv_new_uri(World_val(_world), String_val(_uri));
  caml_alloc_gc_node(v, &ans);
  CAMLreturn (ans);
}

/** Free an LilvNode.
 */
//void lilv_node_free(LilvNode val);
value caml_lilv_node_free(value _node)
{
  CAMLparam1 (_node);
  lilv_node_free(Node_val(_node));
  CAMLreturn (Val_unit);
}

/** Duplicate an LilvNode.
 */
//LilvNode* lilv_node_duplicate(LilvNode* val);
value caml_lilv_node_duplicate(value _node)
{
  CAMLparam1 (_node);
  CAMLlocal1 (ans);
  LilvNode* v = lilv_node_duplicate(Node_val(_node));
  caml_alloc_gc_node(v, &ans);
  CAMLreturn (ans);
}

/** Return whether two nodes are equivalent.
 */
//bool lilv_node_equals(LilvNode* node, LilvNode* other);
CAMLprim value ocaml_lilv_node_equals(value _node, value _other)
{
  CAMLparam2 (_node, _other);
  bool r = lilv_node_equals(Node_val(_node), Node_val(_other));
  CAMLreturn (Val_bool(r));
}

/** Return this node as a Turtle/SPARQL token.
 * Examples:
 * 	<http://example.org/foo>
 * 	doap:name
 * 	"this is a string"
 * 	1.0
 * 	1
 *
 * Returned string is newly allocation and must be freed by caller.
 */
//char*lilv_node_get_turtle_token(LilvNode* node);
value caml_lilv_node_get_turtle_token(value _node)
{
  CAMLparam1 (_node);
  char* v = lilv_node_get_turtle_token(Node_val(_node));
  value r = caml_copy_string(v);
  free(v);
  CAMLreturn (r);
}

/** Return whether the node is a URI (resource).
 *
 * Time = O(1)
 */
//bool lilv_node_is_uri(LilvNode* node);
CAMLprim value ocaml_lilv_node_is_uri(value _node)
{
  CAMLparam1 (_node);
  bool r = lilv_node_is_uri(Node_val(_node));
  CAMLreturn (Val_bool(r));
}

/** Return this node as a URI string, e.g. "http://example.org/foo".
 *
 * Valid to call only if lilv_node_is_uri(\a node) returns true.
 * Returned node is owned by \a node and must not be freed by caller.
 *
 * Time = O(1)
 */
//const char*lilv_node_as_uri(LilvNode* node);
value caml_lilv_node_as_uri(value _node)
{
  CAMLparam1 (_node);
  const char* v = lilv_node_as_uri(Node_val(_node));
  value r = caml_copy_string(v);
  CAMLreturn (r);
}

/** Return whether this node is a literal (i.e. not a URI).
 *
 * Returns true if \a node is a string or numeric node.
 *
 * Time = O(1)
 */
//bool lilv_node_is_literal(LilvNode* node);
CAMLprim value ocaml_lilv_value_is_literal(value _node)
{
  CAMLparam1 (_node);
  bool r = lilv_node_is_literal(Node_val(_node));
  CAMLreturn (Val_bool(r));
}

/** Return whether this node is a string literal.
 *
 * Returns true if \a node is a string (but not  numeric) node.
 *
 * Time = O(1)
 */
//bool lilv_node_is_string(LilvNode* node);
CAMLprim value ocaml_lilv_node_is_string(value _node)
{
  CAMLparam1 (_node);
  bool r = lilv_node_is_string(Node_val(_node));
  CAMLreturn (Val_bool(r));
}

CAMLprim value ocaml_lilv_node_as_string(value _node)
{
  CAMLparam1 (_node);
  const char* v = lilv_node_as_string(Node_val(_node));
  CAMLreturn (caml_copy_string(v));
}

value ocaml_lilv_node_to_string(const LilvNode * node)
{
  const char* s = node ? lilv_node_as_string(node) : "";
  return caml_copy_string(s);
}

value ocaml_lilv_node_to_free_to_string(LilvNode * node)
{
  CAMLparam0();
  CAMLlocal1 (ans);
  ans = ocaml_lilv_node_to_string(node);
  lilv_node_free(node);
  CAMLreturn (ans);
}

value ocaml_lilv_nodes_to_strings(const LilvNodes* nodes)
{
  CAMLparam0();
  CAMLlocal2(v, ans);
  unsigned nodes_size = lilv_nodes_size(nodes);

  ans = caml_alloc_tuple(nodes_size);

  int i = 0;
  LILV_FOREACH(nodes, iter, nodes) {

    const LilvNode* node = lilv_nodes_get(nodes, iter);
    
    Store_field(ans, i, ocaml_lilv_node_to_string(node));
    i++;
  }

  CAMLreturn(ans);
}

/** Return whether this node is a decimal literal.
 *
 * Time = O(1)
 */
//bool lilv_node_is_float(LilvNode* node);
CAMLprim value ocaml_lilv_node_is_float(value _node)
{
  CAMLparam1 (_node);
  bool r = lilv_node_is_float(Node_val(_node));
  CAMLreturn (Val_bool(r));
}


/** Return \a node as a float.
 *
 * Valid to call only if lilv_node_is_float(\a node) or
 * lilv_node_is_int(\a node) returns true.
 *
 * Time = O(1)
 */
//float lilv_node_as_float(LilvNode* node);
CAMLprim value ocaml_lilv_node_as_float(value _node)
{
  CAMLparam1 (_node);
  float v = lilv_node_as_float(Node_val(_node));
  CAMLreturn (caml_copy_double(v));
}


/** Return whether this node is an integer literal.
 *
 * Time = O(1)
 */
//bool lilv_node_is_int(LilvNode* node);
CAMLprim value ocaml_lilv_node_is_int(value _node)
{
  CAMLparam1 (_node);
  bool r = lilv_node_is_int(Node_val(_node));
  CAMLreturn (Val_bool(r));
}


/** Return \a node as an integer.
 *
 * Valid to call only if lilv_node_is_int(\a node) returns true.
 *
 * Time = O(1)
 */
//int lilv_node_as_int(LilvNode* node);
CAMLprim value ocaml_lilv_node_as_int(value _node)
{
  CAMLparam1 (_node);
  int v = lilv_node_as_int(Node_val(_node));
  CAMLreturn (Val_int(v));
}
