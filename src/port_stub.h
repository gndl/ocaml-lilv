/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __OCAML_LILV_PORT_STUB_H_
#define __OCAML_LILV_PORT_STUB_H_

#include "ocaml_lilv.h"
#include "plugin_stub.h"

#define Port_val(v) OwnedData_val(LilvPort, v)
#define PortPlugin_val(v) Plugin_val(Owner_val(v))
#define PortWorldVal_val(v) Owner_val(Owner_val(v))

#define ocaml_lilv_alloc_port(_plugin, port) ocaml_lilv_alloc_owned_data(_plugin, port)
  
#endif /*__OCAML_LILV_PORT_STUB_H_*/
