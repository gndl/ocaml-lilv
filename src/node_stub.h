/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __OCAML_LILV_NODE_STUB_H_
#define __OCAML_LILV_NODE_STUB_H_

#include <stdbool.h>

#include <caml/mlvalues.h>
#include "lilv/lilv.h"

/* Accessing the LilvNode part of a Caml custom block */
#define Node_val(v) (*((LilvNode**) Data_custom_val(v)))
#define ConstNode_val(v) (*((const LilvNode**) Data_custom_val(v)))

/* Allocating a Caml custom block to hold the given LilvNode */
void caml_alloc_node(const LilvNode* node, value * pvalue);
void caml_alloc_gc_node(const LilvNode* node, value * pvalue);
void caml_alloc_node_list(const LilvNodes* nodes, bool gc, value * pvalue);

value caml_alloc_value(const LilvNode* node);
value ocaml_lilv_alloc_node_to_free(const LilvNode* node);
value caml_alloc_value_list(const LilvNodes* nodes, bool gc);

value ocaml_lilv_node_to_string(const LilvNode* node);
value ocaml_lilv_node_to_free_to_string(LilvNode* node);
value ocaml_lilv_nodes_to_strings(const LilvNodes* nodes);

#endif /*__OCAML_LILV_NODE_STUB_H_*/
