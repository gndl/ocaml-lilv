type t


external get_name : t -> string = "ocaml_lilv_plugin_get_name"

external get_class : t -> Pluginclass.t = "ocaml_lilv_plugin_get_class"

external get_uri : t -> string = "ocaml_lilv_plugin_get_uri"

external get_ports : t -> Port.stub_t array = "ocaml_lilv_plugin_get_ports"

external get_port_by_index : t -> int -> Port.t = "ocaml_lilv_plugin_get_port_by_index"

external get_port_by_symbol : t -> string -> Port.t = "ocaml_lilv_plugin_get_port_by_symbol"

let get_ports plugin = Array.map Port.make (get_ports plugin)

external get_num_ports : t -> int = "ocaml_lilv_plugin_get_num_ports"

external get_port_ranges_float : t -> Data.floats_t * Data.floats_t * Data.floats_t  = "ocaml_lilv_plugin_get_port_ranges_float"


external _instantiate : t -> float -> Feature.t array -> Instance.stub = "ocaml_lilv_plugin_instantiate"

let instantiate plugin sample_rate features =
  try
    let stub = _instantiate plugin sample_rate (Array.of_list features) in
    Ok (Instance.make stub features)
  with Failure msg ->
    Error(`Lilv_plugin_instantiation_failure ("Lilv.Plugin " ^ get_name plugin ^ " " ^ msg))
