/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/callback.h>
#include <caml/bigarray.h>
#include <caml/threads.h>

#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "node_stub.h"
#include "port_stub.h"
#include "pluginclass_stub.h"
#include "ui_stub.h"
#include "plugin_stub.h"

value ocaml_lilv_alloc_plugin_array(value _world, const LilvPlugins* plugins)
{
  CAMLparam1(_world);
  CAMLlocal2(v, ans);

  unsigned plugins_size = lilv_plugins_size(plugins);

  ans = caml_alloc_tuple(plugins_size);

  int i = 0;
  LILV_FOREACH(plugins, iter, plugins) {

    const LilvPlugin* plugin = lilv_plugins_get(plugins, iter);
    
    Store_field(ans, i, ocaml_lilv_alloc_owned_data(_world, plugin));
    i++;
  }

  CAMLreturn(ans);
}

//bool lilv_plugin_verify(LilvPlugin* plugin);
value caml_lilv_plugin_verify(value v_plugin)
{
  CAMLparam1 (v_plugin);
  CAMLreturn (Val_bool(lilv_plugin_verify(Plugin_val(v_plugin))));
}

// LilvNode*lilv_plugin_get_uri(LilvPlugin* plugin);
CAMLprim value ocaml_lilv_plugin_get_uri(value _plugin)
{
  CAMLparam1 (_plugin);
  const LilvNode * node = lilv_plugin_get_uri(Plugin_val(_plugin));
  CAMLreturn (ocaml_lilv_node_to_string(node));
}


// LilvNode*lilv_plugin_get_bundle_uri(LilvPlugin* plugin);
value caml_lilv_plugin_get_bundle_uri(value v_plugin)
{
  CAMLparam1 (v_plugin);
  CAMLlocal1 (ans);
  const LilvNode*r = lilv_plugin_get_bundle_uri(Plugin_val(v_plugin));
  caml_alloc_node(r, &ans);
  CAMLreturn (ans);
}

// LilvNodes* lilv_plugin_get_data_uris(LilvPlugin* plugin);
value caml_lilv_plugin_get_data_uris(value v_plugin)
{
  CAMLparam1 (v_plugin);
  CAMLlocal1 (ans);
  const LilvNodes* nodes = lilv_plugin_get_data_uris(Plugin_val(v_plugin));
  caml_alloc_node_list(nodes, false, &ans);
  CAMLreturn (ans);
}

// LilvNode*lilv_plugin_get_library_uri(LilvPlugin* plugin);
value caml_lilv_plugin_get_library_uri(value v_plugin)
{
  CAMLparam1 (v_plugin);
  CAMLlocal1 (ans);
  const LilvNode*r = lilv_plugin_get_library_uri(Plugin_val(v_plugin));
  caml_alloc_node(r, &ans);
  CAMLreturn (ans);
}

CAMLprim value ocaml_lilv_plugin_get_name(value _plugin)
{
  CAMLparam1 (_plugin);
  LilvNode*node = lilv_plugin_get_name(Plugin_val(_plugin));
  CAMLreturn (ocaml_lilv_node_to_free_to_string(node));
}

// LilvPluginClass* lilv_plugin_get_class(LilvPlugin* plugin);
CAMLprim value ocaml_lilv_plugin_get_class(value _plugin)
{
  CAMLparam1 (_plugin);
  CAMLlocal1 (ans);
  const LilvPluginClass* r = lilv_plugin_get_class(Plugin_val(_plugin));
  ans = ocaml_lilv_alloc_plugin_class(Owner_val(_plugin), r);
  CAMLreturn (ans);
}

// LilvNodes* lilv_plugin_get_value(LilvPlugin* p, LilvNode*predicate);
value caml_lilv_plugin_get_value(value v_plugin, value v_predicate)
{
  CAMLparam2 (v_plugin, v_predicate);
  LilvNodes* nodes = lilv_plugin_get_value(Plugin_val(v_plugin), Node_val(v_predicate));
  value r = caml_alloc_value_list(nodes, false);
  lilv_nodes_free(nodes);
  CAMLreturn (r);
}

// bool lilv_plugin_has_feature(LilvPlugin* p, LilvNode*feature_uri);
value caml_lilv_plugin_has_feature(value v_plugin, value v_feature_uri)
{
  CAMLparam2 (v_plugin, v_feature_uri);
  bool r = lilv_plugin_has_feature(Plugin_val(v_plugin), Node_val(v_feature_uri));
  CAMLreturn (Val_bool(r));
}

// LilvNodes* lilv_plugin_get_supported_features(LilvPlugin* p);
value caml_lilv_plugin_get_supported_features(value v_plugin)
{
  CAMLparam1 (v_plugin);
  LilvNodes* nodes = lilv_plugin_get_supported_features(Plugin_val(v_plugin));
  value r = caml_alloc_value_list(nodes, false);
  lilv_nodes_free(nodes);
  CAMLreturn (r);
}

// LilvNodes* lilv_plugin_get_required_features(LilvPlugin* p);
value caml_lilv_plugin_get_required_features(value v_plugin)
{
  CAMLparam1 (v_plugin);
  LilvNodes* nodes = lilv_plugin_get_required_features(Plugin_val(v_plugin));
  value r = caml_alloc_value_list(nodes, false);
  lilv_nodes_free(nodes);
  CAMLreturn (r);
}

// LilvNodes* lilv_plugin_get_optional_features(LilvPlugin* p);
value caml_lilv_plugin_get_optional_features(value v_plugin)
{
  CAMLparam1 (v_plugin);
  LilvNodes* nodes = lilv_plugin_get_optional_features(Plugin_val(v_plugin));
  value r = caml_alloc_value_list(nodes, false);
  lilv_nodes_free(nodes);
  CAMLreturn (r);
}

CAMLprim value ocaml_lilv_plugin_get_num_ports(value _plugin)
{
  CAMLparam1 (_plugin);
  int r = (int)lilv_plugin_get_num_ports(Plugin_val(_plugin));
  CAMLreturn (Val_int(r));
}

CAMLprim value ocaml_lilv_plugin_get_port_ranges_float(value _plugin)
{
  CAMLparam1 (_plugin);
  CAMLlocal4 (min_values, max_values, def_values, r);
  LilvPlugin* plugin = Plugin_val(_plugin);

  uint32_t num_ports = lilv_plugin_get_num_ports(plugin);

  min_values = caml_ba_alloc_dims(CAML_BA_FLOAT32 | CAML_BA_C_LAYOUT, 1, NULL, num_ports);
  max_values = caml_ba_alloc_dims(CAML_BA_FLOAT32 | CAML_BA_C_LAYOUT, 1, NULL, num_ports);
  def_values = caml_ba_alloc_dims(CAML_BA_FLOAT32 | CAML_BA_C_LAYOUT, 1, NULL, num_ports);

  lilv_plugin_get_port_ranges_float(plugin,
                                    (float*)Caml_ba_data_val(min_values),
                                    (float*)Caml_ba_data_val(max_values),
                                    (float*)Caml_ba_data_val(def_values));

  r = caml_alloc_tuple(3);
  Store_field(r, 0, min_values);
  Store_field(r, 1, max_values);
  Store_field(r, 2, def_values);

  CAMLreturn (r);
}

// uint32_t lilv_plugin_get_num_ports_of_class(LilvPlugin* p, LilvNode* class_1, ...);
value caml_lilv_plugin_get_num_ports_of_class(value v_plugin, value v_classes)
{
  CAMLparam2 (v_plugin, v_classes);
  CAMLlocal1(v);
  LilvPlugin* plugin = Plugin_val(v_plugin);
  uint32_t r = 0;
   
  v = v_classes;
   
  while(v != Val_int(0)) {
    r += lilv_plugin_get_num_ports_of_class(plugin, Node_val(Field(v, 0)), NULL);
    v = Field(v, 1);
  }

  CAMLreturn (Val_int((int)r));
}

// bool lilv_plugin_has_latency(LilvPlugin* p);
value caml_lilv_plugin_has_latency(value v_plugin)
{
  CAMLparam1 (v_plugin);
  bool r = lilv_plugin_has_latency(Plugin_val(v_plugin));
  CAMLreturn (Val_bool(r));
}

// uint32_t lilv_plugin_get_latency_port_index(LilvPlugin* p);
value caml_lilv_plugin_get_latency_port_index(value v_plugin)
{
  CAMLparam1 (v_plugin);
  uint32_t r = lilv_plugin_get_latency_port_index(Plugin_val(v_plugin));
  CAMLreturn (Val_int((int)r));
}

CAMLprim value ocaml_lilv_plugin_get_ports(value _plugin)
{
  CAMLparam1 (_plugin);
  CAMLlocal1(ans);

  const LilvPlugin* plugin = Plugin_val(_plugin);
  uint32_t num_ports = lilv_plugin_get_num_ports(plugin);
  
  ans = caml_alloc_tuple(num_ports);

  uint32_t i = 0;

  for(; i < num_ports; i++) {
    const LilvPort* port = lilv_plugin_get_port_by_index(plugin, i);

    Store_field(ans, i, ocaml_lilv_alloc_owned_data(_plugin, port));
  }

  CAMLreturn(ans);
}

// LilvPort* lilv_plugin_get_port_by_index(LilvPlugin* plugin, uint32_t index);
CAMLprim value ocaml_lilv_plugin_get_port_by_index(value _plugin, value _index)
{
  CAMLparam1 (_plugin);
  const LilvPort* p = lilv_plugin_get_port_by_index(Plugin_val(_plugin), (uint32_t)Int_val(_index));
  CAMLreturn (ocaml_lilv_alloc_port(_plugin, p));
}

// LilvPort* lilv_plugin_get_port_by_symbol(LilvPlugin* plugin, LilvNode* symbol);
CAMLprim value ocaml_lilv_plugin_get_port_by_symbol(value _plugin, value _symbol)
{
  CAMLparam2 (_plugin, _symbol);
  const LilvPort* p = lilv_plugin_get_port_by_symbol(Plugin_val(_plugin), Node_val(_symbol));
  CAMLreturn (ocaml_lilv_alloc_port(_plugin, p));
}

// LilvUIs* lilv_plugin_get_uis(LilvPlugin* plugin);
value caml_lilv_plugin_get_uis(value v_plugin)
{
  CAMLparam1 (v_plugin);
  LilvUIs* uis = lilv_plugin_get_uis(Plugin_val(v_plugin));
  CAMLreturn (caml_alloc_ui_list(uis));
}

// LilvNode* lilv_plugin_get_author_name(LilvPlugin* plugin);
value caml_lilv_plugin_get_author_name(value v_plugin)
{
  CAMLparam1 (v_plugin);
  LilvNode* r = lilv_plugin_get_author_name(Plugin_val(v_plugin));
  CAMLreturn (ocaml_lilv_alloc_node_to_free(r));
}

// LilvNode* lilv_plugin_get_author_email(LilvPlugin* plugin);
value caml_lilv_plugin_get_author_email(value v_plugin)
{
  CAMLparam1 (v_plugin);
  LilvNode* r = lilv_plugin_get_author_email(Plugin_val(v_plugin));
  CAMLreturn (ocaml_lilv_alloc_node_to_free(r));
}

// LilvNode* lilv_plugin_get_author_homepage(LilvPlugin* plugin);
value caml_lilv_plugin_get_author_homepage(value v_plugin)
{
  CAMLparam1 (v_plugin);
  LilvNode* r = lilv_plugin_get_author_homepage(Plugin_val(v_plugin));
  CAMLreturn (ocaml_lilv_alloc_node_to_free(r));
}



