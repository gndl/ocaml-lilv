type kind_t = [
  | `AtomPort
  | `AudioPort
  | `ControlPort
  | `CVPort
  | `EventPort
  | `OtherPort
]

type flow_t = [
  | `InputPort
  | `OutputPort
]

type class_t = [kind_t | flow_t]

type stub_t

type t = {
  stub : stub_t;
  kind : kind_t;
  flow : flow_t;
  is_connection_optional : bool;
  is_toggled : bool;
  has_strict_bounds : bool;
  is_logarithmic : bool;
  is_trigger : bool;
  is_integer : bool;
  is_enumeration : bool;
}

external _get_classes : stub_t -> string array = "ocaml_lilv_port_get_classes"
let get_classes p = _get_classes p.stub

external _is_a : stub_t -> Lv2.id_t -> bool = "ocaml_lilv_port_is_a"
let is_a p id = _is_a p.stub id

external _get : stub_t -> Lv2.id_t -> Node.t option = "ocaml_lilv_port_get"
let get p id = _get p.stub id

external _get_properties : stub_t -> string array = "ocaml_lilv_port_get_properties"
let get_properties p = _get_properties p.stub

external _has_property : stub_t -> Lv2.id_t -> bool = "ocaml_lilv_port_has_property"
let has_property p id = _has_property p.stub id

let make stub =
  {
    stub;
    kind = if _is_a stub Lv2.Core_AudioPort then `AudioPort
      else if _is_a stub Lv2.Core_ControlPort then `ControlPort
      else if _is_a stub Lv2.Core_CVPort then `CVPort
      else if _is_a stub Lv2.Atom_AtomPort then `AtomPort
      else if _is_a stub Lv2.Event_EventPort then `EventPort
      else `OtherPort;
    flow = if _is_a stub Lv2.Core_InputPort then `InputPort else `OutputPort;
    is_connection_optional = _has_property stub Lv2.Core_connectionOptional;
    is_toggled = _has_property stub Lv2.Core_toggled;
    has_strict_bounds = _has_property stub Lv2.PortProps_hasStrictBounds;
    is_logarithmic = _has_property stub Lv2.PortProps_logarithmic;
    is_trigger = _has_property stub Lv2.PortProps_trigger;
    is_integer = _has_property stub Lv2.Core_integer;
    is_enumeration = _has_property stub Lv2.Core_enumeration;
  }


external get_name : stub_t -> string = "ocaml_lilv_port_get_name"
let get_name p = get_name p.stub

external get_symbol : stub_t -> string = "ocaml_lilv_port_get_symbol"
let get_symbol p = get_symbol p.stub

external get_range : stub_t -> float * float * float = "ocaml_lilv_port_get_range"
let get_range p = get_range p.stub


let get_kind p = p.kind
let get_flow p = p.flow
let is_connection_optional p = p.is_connection_optional
let is_toggled p = p.is_toggled
let has_strict_bounds p = p.has_strict_bounds
let is_logarithmic p = p.is_logarithmic
let is_trigger p = p.is_trigger
let is_integer p = p.is_integer
let is_enumeration p = p.is_enumeration
