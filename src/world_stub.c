/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>
#include <caml/callback.h>
#include <caml/custom.h>
#include <caml/threads.h>

#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "node_stub.h"
#include "pluginclass_stub.h"
#include "plugin_stub.h"
#include "world_stub.h"

static void custom_finalize_world (value v)
{
  OcamlWorld * ow = OcamlWorld_val(v);
  
  for(int i = 0; i < OCAML_LILV_LV2_NODES_LEN; i++) {
    if(ow->nodes[i]) lilv_node_free(ow->nodes[i]);
  }

  lilv_world_free(ow->lilvWorld);
  free(ow);
}

// Encapsulation of opaque world handles (of type LilvWorld) as Caml custom blocks.
static struct custom_operations lilv_world_ops =
  {
   "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/world",
   custom_finalize_world,
   custom_compare_default,
   custom_hash_default,
   custom_serialize_default,
   custom_deserialize_default
  };

/* Allocating a Caml custom block to hold the given LilvWorld */
static void ocaml_lilv_alloc_world(LilvWorld* world, value * pvalue)
{
  OcamlWorld * ow = (OcamlWorld *)calloc(1, sizeof(OcamlWorld));
  ow->lilvWorld = world;
  
  *pvalue = caml_alloc_custom(&lilv_world_ops, sizeof(OcamlWorld*), 0, 1);

  OcamlWorld_val(*pvalue) = ow;
}

const LilvNode * ocaml_world_get_node_by_id(OcamlWorld * ow, int id)
{
  if(!ow->nodes[id]) {
    ow->nodes[id] = lilv_new_uri(ow->lilvWorld, ocaml_lilv_lv2_uri(id));
  }
  
  return ow->nodes[id];
}

value caml_lilv_world_new (value unit)
{
  CAMLparam1 (unit);
  CAMLlocal1(ans);

  caml_release_runtime_system();
  LilvWorld* w = lilv_world_new();
  caml_acquire_runtime_system();
   
  if(w == NULL) caml_failwith("Lilv.World initialization failed");

  ocaml_lilv_alloc_world(w, &ans);
  
  CAMLreturn (ans);
}

//void lilv_world_set_option(LilvWorld* world, const char* uri, const LilvNode* value);
value caml_lilv_world_set_option(value v_world, value v_uri, value v_value)
{
  CAMLparam3 (v_world, v_uri, v_value);
  lilv_world_set_option(World_val(v_world), String_val(v_uri), Node_val(v_value));
  CAMLreturn (Val_unit);
}

CAMLprim value ocaml_lilv_world_load_all()
{
  CAMLparam0();
  CAMLlocal1(ans);
   
  caml_release_runtime_system();
  LilvWorld* world = lilv_world_new();

  if(world) {
    lilv_world_load_all(world);
  }
  caml_acquire_runtime_system();

  if(!world) {
    caml_failwith("Lilv.World initialization failed");
  }

  ocaml_lilv_alloc_world(world, &ans);
  
  CAMLreturn (ans);
}

CAMLprim value ocaml_lilv_world_get_all_plugins(value _world)
{
  CAMLparam1 (_world);
  CAMLlocal1(ans);
  LilvWorld* world = World_val(_world);

  caml_release_runtime_system();
  const LilvPlugins* plugins = lilv_world_get_all_plugins(world);
  caml_acquire_runtime_system();

  ans = ocaml_lilv_alloc_plugin_array(_world, plugins);
  
  CAMLreturn (ans);
}

value caml_lilv_world_load_bundle(value v_world, value v_bundle_uri)
{
  CAMLparam2 (v_world, v_bundle_uri);
  lilv_world_load_bundle(World_val(v_world), Node_val(v_bundle_uri));
  CAMLreturn (Val_unit);
}

//int lilv_world_load_resource(LilvWorld* world, const LilvNode* resource);
value caml_lilv_world_load_resource(value v_world, value v_resource)
{
  CAMLparam2 (v_world, v_resource);
  int v = lilv_world_load_resource(World_val(v_world), Node_val(v_resource));
  CAMLreturn (Val_int(v));
}

//LilvPluginClass* lilv_world_get_plugin_class(LilvWorld* world);
CAMLprim value ocaml_lilv_world_get_plugin_class(value _world)
{
  CAMLparam1 (_world);
  CAMLlocal1 (ans);
  const LilvPluginClass* pc = lilv_world_get_plugin_class(World_val(_world));
  ans = ocaml_lilv_alloc_plugin_class(_world, pc);
  CAMLreturn (ans);
}

//LilvPluginClasses* lilv_world_get_plugin_classes(LilvWorld* world);
CAMLprim value ocaml_lilv_world_get_plugin_classes(value _world)
{
  CAMLparam1 (_world);
  CAMLlocal1 (ans);
  const LilvPluginClasses* pcs = lilv_world_get_plugin_classes(World_val(_world));
  ans = ocaml_lilv_alloc_plugin_class_array(_world, pcs);
  CAMLreturn (ans);
}

// TODO : option
//LilvNodes* lilv_world_find_nodes(LilvWorld* world, const LilvNode* subject, const LilvNode* predicate, const LilvNode* object);
value caml_lilv_world_find_nodes(value v_world, value v_subject, value v_predicate, value v_object)
{
  CAMLparam4 (v_world, v_subject, v_predicate, v_object);
  CAMLlocal1 (ans);
  LilvWorld* world = World_val(v_world);
  LilvNode* subject = Node_val(v_subject);
  LilvNode* predicate = Node_val(v_predicate);
  LilvNode* object = Node_val(v_object);

  LilvNodes* ns = lilv_world_find_nodes(world, subject, predicate, object);
  caml_alloc_node_list(ns, true, &ans);

  lilv_nodes_free(ns);
  CAMLreturn (ans);
}


