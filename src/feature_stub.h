/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __OCAML_LILV_FEATURE_STUB_H__
#define __OCAML_LILV_FEATURE_STUB_H__

#include "lilv/lilv.h"

typedef struct {
  LV2_Feature lv2;
  value associated_value;
} OcamlLv2Feature;

/* Accessing the OcamlLv2FeatureOcamlLv2Feature part of a Caml custom block */
#define Feature_val(v) (*((OcamlLv2Feature**) Data_custom_val(v)))
#define Lv2Feature_val(v) ((LV2_Feature*)Feature_val(v))

#endif /* __OCAML_LILV_FEATURE_STUB_H__ */
