type t

external _lv2 : Lv2.id_t -> t = "ocaml_lilv_feature_lv2"

let lv2 id =
  try
    Ok(_lv2 id)
  with Failure msg -> Error(`Lilv_feature_failure msg)


external _log : (int -> string -> unit) -> t = "ocaml_lilv_feature_log"

let log callback =
  try
    Ok(_log callback)
  with Failure msg -> Error(`Lilv_feature_failure msg)


external _default_urid_map : unit -> t = "ocaml_lilv_feature_default_urid_map"

let default_urid_map() =
  try
    Ok(_default_urid_map())
  with Failure msg -> Error(`Lilv_feature_failure msg)

external _urid_map : (string -> int) -> t = "ocaml_lilv_feature_urid_map"

let urid_map callback =
  try
    Ok(_urid_map callback)
  with Failure msg -> Error(`Lilv_feature_failure msg)


external _default_urid_unmap : unit -> t = "ocaml_lilv_feature_default_urid_unmap"

let default_urid_unmap() =
  try
    Ok(_default_urid_unmap())
  with Failure msg -> Error(`Lilv_feature_failure msg)

external _urid_unmap : (int -> string) -> t = "ocaml_lilv_feature_urid_unmap"

let urid_unmap callback =
  try
    Ok(_urid_unmap callback)
  with Failure msg -> Error(`Lilv_feature_failure msg)
