/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>

#include <stdbool.h>
#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "world_stub.h"
#include "node_stub.h"
#include "scalepoint_stub.h"

// Encapsulation of opaque scalepoint handles (of type LilvScalePoint) as Caml custom blocks.
static struct custom_operations lilv_scalepoint_ops = {
  "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/scalepoint",
  custom_finalize_default,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

/* Allocating a Caml custom block to hold the given LilvScalePoint */
value caml_alloc_scalepoint(const LilvScalePoint* sp)
{
  value v_sp = caml_alloc_custom(&lilv_scalepoint_ops, sizeof(LilvScalePoint*), sizeof(LilvScalePoint*), 10 * sizeof(LilvScalePoint*));
  ConstScalePoint_val(v_sp) = sp;
  return v_sp;
}

value caml_alloc_scalepoint_list(const LilvScalePoints* sps)
{
  CAMLparam0();
  CAMLlocal3 (firstElem, elem, nextElem);
  unsigned spsSize = lilv_scale_points_size(sps);

  if(spsSize > 0) {
    LilvIter* iter = lilv_scale_points_begin(sps);
    const LilvScalePoint* p = lilv_scale_points_get(sps, iter);

    firstElem = elem = caml_alloc(2, 0);
    Store_field(elem, 0, caml_alloc_scalepoint(p));
    iter = lilv_scale_points_next(sps, iter);

    while( ! lilv_scale_points_is_end(sps, iter)) {

      p = lilv_scale_points_get(sps, iter);

      nextElem = caml_alloc(2, 0);
      Store_field(nextElem, 0, caml_alloc_scalepoint(p));
      Store_field(elem, 1, nextElem);

      elem = nextElem;
      iter = lilv_scale_points_next(sps, iter);
    }
    Store_field(elem, 1, Val_int(0));
  }
  else {
    firstElem = Val_int(0);
  }
  return(firstElem);
}
/*
  value caml_alloc_scalepoint_list(LilvScalePoints* sps)
  {
  CAMLlocal2 (r, r2);
  unsigned i = lilv_scale_points_size(sps);
   
  if(i > 0) {
  LilvScalePoint* p = lilv_scale_points_get_at(sps, i - 1);
  r = caml_alloc(2, 0);
  Store_field(r, 0, caml_alloc_scalepoint(p));
  Store_field(r, 1, Val_int(0));

  for(--i; i > 0; i--)
  {
  p = lilv_scale_points_get_at(sps, i - 1); 
  r2 = r;
  r = caml_alloc(2, 0);
  Store_field(r, 0, caml_alloc_scalepoint(p));
  Store_field(r, 1, r2);
  }
  }
  else {
  r = Val_int(0);
  }
  return(r);
  }
*/
//LilvNode* lilv_scale_point_get_label(LilvScalePoint* point);
value caml_lilv_scale_point_get_label(value v_point)
{
  CAMLparam1 (v_point);
  CAMLlocal1 (ans);
  const LilvNode* v = lilv_scale_point_get_label(ScalePoint_val(v_point));
  caml_alloc_node(v, &ans);
  CAMLreturn (ans);
}

//LilvNode* lilv_scale_point_get_value(LilvScalePoint* point);
value caml_lilv_scale_point_get_value(value v_point)
{
  CAMLparam1 (v_point);
  CAMLlocal1 (ans);
  const LilvNode* v = lilv_scale_point_get_value(ScalePoint_val(v_point));
  caml_alloc_node(v, &ans);
  CAMLreturn (ans);
}
