/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <math.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/callback.h>
#include <caml/bigarray.h>

#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "world_stub.h"
#include "node_stub.h"
#include "plugin_stub.h"
#include "scalepoint_stub.h"
#include "port_stub.h"

//LilvNodes* lilv_port_get_value(LilvPlugin plugin, LilvPort* port, LilvNode*predicate);
value caml_lilv_port_get_value(value _plugin, value _port, value _predicate)
{
  CAMLparam3 (_plugin, _port, _predicate);
  LilvNodes* vs = lilv_port_get_value(Plugin_val(_plugin), Port_val(_port), Node_val(_predicate));
  value r = caml_alloc_value_list(vs, false);
  lilv_nodes_free(vs);
  CAMLreturn (r);
}

CAMLprim value ocaml_lilv_port_get(value _port, value _id)
{
  CAMLparam1 (_port);
  CAMLlocal1 (ans);

  OcamlWorld * ow = OcamlWorld_val(PortWorldVal_val(_port));
  const LilvNode * id_node = ocaml_world_get_node_by_id(ow, Int_val(_id));

  LilvNode * node = lilv_port_get(PortPlugin_val(_port), Port_val(_port), id_node);

  if(node) {
    ans = caml_alloc(1, 0);
    Store_field(ans, 0, ocaml_lilv_alloc_node_to_free(node));
  }
  else {
    ans = Val_int(0);
  }
  CAMLreturn (ans);
}

//LilvNodes* lilv_port_get_properties(LilvPlugin plugin, LilvPort* port);
CAMLprim value ocaml_lilv_port_get_properties(value _port)
{
  CAMLparam1 (_port);
  CAMLlocal1 (ans);
  LilvNodes* ns = lilv_port_get_properties(PortPlugin_val(_port), Port_val(_port));
  ans = ocaml_lilv_nodes_to_strings(ns);
  lilv_nodes_free(ns);
  CAMLreturn (ans);
}

CAMLprim value ocaml_lilv_port_has_property(value _port, value _id)
{
  CAMLparam1 (_port);
  OcamlWorld * ow = OcamlWorld_val(PortWorldVal_val(_port));
  const LilvNode * property_uri = ocaml_world_get_node_by_id(ow, Int_val(_id));

  bool ans = lilv_port_has_property(PortPlugin_val(_port), Port_val(_port), property_uri);

  CAMLreturn (Val_bool(ans));
}

//bool lilv_port_supports_event(LilvPlugin p, LilvPort* port, LilvNode*event_uri);
value caml_lilv_port_supports_event(value _plugin, value _port, value _event_uri)
{
  CAMLparam3 (_plugin, _port, _event_uri);
  bool r = lilv_port_supports_event(Plugin_val(_plugin), Port_val(_port), Node_val(_event_uri));
  CAMLreturn (Val_bool(r));
}

CAMLprim value ocaml_lilv_port_get_symbol(value _port)
{
  CAMLparam1 (_port);
  const LilvNode * n = lilv_port_get_symbol(PortPlugin_val(_port), Port_val(_port));
  CAMLreturn (ocaml_lilv_node_to_string(n));
}

CAMLprim value ocaml_lilv_port_get_name(value _port)
{
  CAMLparam1 (_port);
  LilvNode * n = lilv_port_get_name(PortPlugin_val(_port), Port_val(_port));
  CAMLreturn (ocaml_lilv_node_to_free_to_string(n));
}

CAMLprim value ocaml_lilv_port_get_classes(value _port)
{
  CAMLparam1 (_port);
  const LilvNodes* ns = lilv_port_get_classes(PortPlugin_val(_port), Port_val(_port));
  CAMLreturn (ocaml_lilv_nodes_to_strings(ns));
}

CAMLprim value ocaml_lilv_port_is_a(value _port, value _id)
{
  CAMLparam1 (_port);
  OcamlWorld * ow = OcamlWorld_val(PortWorldVal_val(_port));
  const LilvNode * port_class = ocaml_world_get_node_by_id(ow, Int_val(_id));

  bool ans = lilv_port_is_a(PortPlugin_val(_port), Port_val(_port), port_class);

  CAMLreturn (Val_bool(ans));
}

//void lilv_port_get_range(LilvPlugin plugin, LilvPort* port);
CAMLprim value ocaml_lilv_port_get_range(value _port)
{
  CAMLparam1 (_port);
  CAMLlocal1 (ans);
  LilvNode* def = NULL; LilvNode* min = NULL; LilvNode* max = NULL;
  float fdef = NAN; float fmin = NAN; float fmax = NAN;

  lilv_port_get_range(PortPlugin_val(_port), Port_val(_port), &def, &min, &max);

  if(def) {
    fdef = lilv_node_as_float(def);
    lilv_node_free(def);
  }

  if(min) {
    fmin = lilv_node_as_float(min);
    lilv_node_free(min);
  }

  if(max) {
    fmax = lilv_node_as_float(max);
    lilv_node_free(max);
  }

  ans = caml_alloc_tuple(3);

  Store_field(ans, 0, caml_copy_double(fdef));
  Store_field(ans, 1, caml_copy_double(fmin));
  Store_field(ans, 2, caml_copy_double(fmax));

  CAMLreturn (ans);
}

//LilvScalePoints* lilv_port_get_scale_points(LilvPlugin plugin, LilvPort* port);
value caml_lilv_port_get_scale_points(value _plugin, value _port)
{
  CAMLparam2 (_plugin, _port);
  LilvScalePoints* sps = lilv_port_get_scale_points(Plugin_val(_plugin), Port_val(_port));
  value ans = caml_alloc_scalepoint_list(sps);
  if (sps) lilv_scale_points_free(sps);
  CAMLreturn (ans);
}
