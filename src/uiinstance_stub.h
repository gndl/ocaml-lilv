/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __OCAML_LILV_PLUGINUIINSTANCE_STUB_H__
#define __OCAML_LILV_PLUGINUIINSTANCE_STUB_H__

#include <caml/mlvalues.h>
#include "lilv/lilv.h"

/* Accessing the LilvUIInstance part of a Caml custom block */
#define UIInstance_val(v) (*((LilvUIInstance**) Data_custom_val(v)))

value caml_alloc_ui_instance(LilvUIInstance* ui_instance);

#endif /* __OCAML_LILV_PLUGINUIINSTANCE_STUB_H__ */
