/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __OCAML_LILV_WORLD_STUB_H_
#define __OCAML_LILV_WORLD_STUB_H_

#include <caml/mlvalues.h>
#include "lilv/lilv.h"
#include "lv2_stub.h"

typedef struct {
  LilvWorld * lilvWorld;
  LilvNode * nodes[OCAML_LILV_LV2_NODES_LEN];
} OcamlWorld;

/* Accessing the LilvWorld part of a Caml custom block */
#define OcamlWorld_val(v) (*((OcamlWorld**) Data_custom_val(v)))
#define World_val(v) (OcamlWorld_val(v)->lilvWorld)

const LilvNode * ocaml_world_get_node_by_id(OcamlWorld * ow, int num_node);

#endif /*__OCAML_LILV_WORLD_STUB_H_*/
