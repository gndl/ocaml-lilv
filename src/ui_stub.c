/* Stub code to access LILV functions from OCaml */

/*
** Copyright (c) 2019 Gaetan Dubreil
** WWW:
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/custom.h>

#include <stdbool.h>
#include "lilv/lilv.h"

#include "ocaml_lilv.h"
#include "node_stub.h"
#include "ui_stub.h"

// Encapsulation of opaque ui handles (of type LilvUI) as Caml custom blocks.
static struct custom_operations lilv_ui_ops = {
  "LILV/OCAMLinterface/" OCAML_LILV_VERSION "/ui",
  custom_finalize_default,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

/* Allocating a Caml custom block to hold the given LilvUI */
value caml_alloc_ui(const LilvUI* ui)
{
  value r = caml_alloc_custom(&lilv_ui_ops, sizeof(LilvUI*), 0, 1);
  ConstUI_val(r) = ui;
  return r;
}

value caml_alloc_ui_list(const LilvUIs* uis)
{
  CAMLparam0();
  CAMLlocal3 (firstElem, elem, nextElem);
  unsigned uisSize = lilv_uis_size(uis);

  if(uisSize > 0) {
    LilvIter* iter = lilv_uis_begin(uis);
    const LilvUI* p = lilv_uis_get(uis, iter);

    firstElem = elem = caml_alloc(2, 0);
    Store_field(elem, 0, caml_alloc_ui(p));
    iter = lilv_uis_next(uis, iter);

    while( ! lilv_uis_is_end(uis, iter)) {

      p = lilv_uis_get(uis, iter);

      nextElem = caml_alloc(2, 0);
      Store_field(nextElem, 0, caml_alloc_ui(p));
      Store_field(elem, 1, nextElem);

      elem = nextElem;
      iter = lilv_uis_next(uis, iter);
    }
    Store_field(elem, 1, Val_int(0));
  }
  else {
    firstElem = Val_int(0);
  }
  return(firstElem);
}

value caml_lilv_ui_get_uri(value v_ui)
{
  CAMLparam1 (v_ui);
  CAMLlocal1 (ans);
  const LilvNode* r = lilv_ui_get_uri(UI_val(v_ui));
  caml_alloc_node(r, &ans);
  CAMLreturn (ans);
}

value caml_lilv_ui_get_classes(value v_ui)
{
  CAMLparam1 (v_ui);
  CAMLlocal1 (ans);
  const LilvNodes* vs = lilv_ui_get_classes(UI_val(v_ui));
  caml_alloc_node_list(vs, false, &ans);
  CAMLreturn (ans);
}

value caml_lilv_ui_is_a(value v_ui, value v_class_uri)
{
  CAMLparam2 (v_ui, v_class_uri);
  bool r = lilv_ui_is_a(UI_val(v_ui), Node_val(v_class_uri));
  CAMLreturn (Val_bool(r));
}

value caml_lilv_ui_get_bundle_uri(value v_ui)
{
  CAMLparam1 (v_ui);
  const LilvNode* r = lilv_ui_get_bundle_uri(UI_val(v_ui));
  CAMLreturn (caml_alloc_value(r));
}

value caml_lilv_ui_get_binary_uri(value v_ui)
{
  CAMLparam1 (v_ui);
  const LilvNode* r = lilv_ui_get_binary_uri(UI_val(v_ui));
  CAMLreturn (caml_alloc_value(r));
}

/*
  value caml_alloc_ui_list(LilvUIs uis)
  {
  CAMLlocal2 (r, r2);
  unsigned i = lilv_uis_size(uis);

  if(i > 0)
  {
  LilvUI p = lilv_uis_get_at(uis, i - 1);
  r = caml_alloc(2, 0);
  Store_field(r, 0, caml_alloc_ui(p));
  Store_field(r, 1, Val_int(0));

  for(--i; i > 0; i--)
  {
  p = lilv_uis_get_at(uis, i - 1);
  r2 = r;
  r = caml_alloc(2, 0);
  Store_field(r, 0, caml_alloc_ui(p));
  Store_field(r, 1, r2);
  }
  }
  else {
  r = Val_int(0);
  }
  return(r);
  }
*/
