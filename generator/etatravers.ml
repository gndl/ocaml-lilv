let get_path filename =
  ("-I/usr/include"::"-I/usr/include/i386-linux-gnu"::(Sys.argv |> Array.to_list))
  |> List.fold_left(fun path param ->
      if path = None && "-I" = String.sub param 0 2 then (
        let p = String.concat "" [(String.sub param 2 (String.length param - 2)); "/"; filename]
        in
        if Sys.file_exists p then Some p
        else (
          None
        )
      )
      else path
    ) None


type params_t = {
  in_name : string;
  c_prefix : string;
  ml_prefix : string;
}

let print words ocs =
  List.iter(fun s -> List.iter(fun oc -> output_string oc s) ocs) words;
  List.iter(fun oc -> output_string oc "\n") ocs


let translate_define_lines ic param c_oc ml_oc mli_oc nodes_count =

  let re = Str.regexp ("#define " ^ param.c_prefix ^"\\([A-Za-z0-9_]+\\)") in

  let rec loop nc =
    match input_line ic with
    | line when Str.string_match re line 0 ->
      let id = Str.matched_group 1 line in

      print [" | "; param.ml_prefix; "_"; id] [ml_oc; mli_oc];
      print [param.c_prefix; id; ","] [c_oc];

      loop nc + 1
    | _ -> loop nc
    | exception End_of_file -> nc
  in
  loop nodes_count


let translate_lv2_defines params =

  let ns = "lv2/lv2plug.in/ns/" in

  let c_oc = open_out "lv2_stub.c" in
  let ml_oc = open_out "lv2.ml" in
  let mli_oc = open_out "lv2.mli" in

  print ["(** LV2 values *)\n\n"; "type id_t ="] [mli_oc; ml_oc];

  List.iter(fun p ->
      let in_name = ns ^ p.in_name in

      match get_path in_name with
      | None -> Printf.eprintf"WARNING : header file %s not found\n" in_name
      | Some _ -> print ["#include \""; in_name; "\""] [c_oc];
    ) params;

  print ["#include <caml/alloc.h>\n";
         "#include \"lv2_stub.h\"\n\n";
         "static const char * OcamlLv2UriTab[] = {"] [c_oc];

  let nodes_count = List.fold_left(fun nc param ->
      let in_name = ns ^ param.in_name in

      match get_path in_name with
      | None -> nc
      | Some path ->
        let ic = open_in path in

        let nc = translate_define_lines ic param c_oc ml_oc mli_oc nc in

        close_in ic;
        nc
    ) 0 params in

  print ["\nval uri : id_t -> string"] [mli_oc];
  print ["\nexternal uri : id_t -> string = \"ocaml_lilv_lv2_uri\""] [ml_oc];

  print["NULL\n};\n\n";
        "const char * ocaml_lilv_lv2_uri(int i)\n{\n";
        "return OcamlLv2UriTab[i];\n}"
       ] [c_oc];

  close_out c_oc;
  close_out mli_oc;
  close_out ml_oc;

  let h_oc = open_out "lv2_stub.h" in

  print["#ifndef __OCAML_LV2_STUB_H_\n#define __OCAML_LV2_STUB_H_\n";
        "#define OCAML_LILV_LV2_NODES_LEN "; string_of_int nodes_count; "\n";
        "const char * ocaml_lilv_lv2_uri(int i);\n";
        "#endif"] [h_oc];

  close_out h_oc


let () =
  translate_lv2_defines [
    {in_name = "lv2core/lv2.h"; c_prefix = "LV2_CORE__"; ml_prefix = "Core"};
    {in_name = "ext/atom/atom.h"; c_prefix = "LV2_ATOM__"; ml_prefix = "Atom"};
    {in_name = "ext/buf-size/buf-size.h"; c_prefix = "LV2_BUF_SIZE__"; ml_prefix = "BufSize"};
    {in_name = "ext/data-access/data-access.h"; c_prefix = "LV2_DATA_ACCESS__"; ml_prefix = "DataAccess"};
    {in_name = "ext/dynmanifest/dynmanifest.h"; c_prefix = "LV2_DYN_MANIFEST__"; ml_prefix = "DynManifest"};
    {in_name = "ext/event/event.h"; c_prefix = "LV2_EVENT__"; ml_prefix = "Event"};
    {in_name = "ext/instance-access/instance-access.h"; c_prefix = "LV2_INSTANCE_ACCESS__"; ml_prefix = "InstanceAccess"};
    {in_name = "ext/log/log.h"; c_prefix = "LV2_LOG__"; ml_prefix = "Log"};
    {in_name = "ext/midi/midi.h"; c_prefix = "LV2_MIDI__"; ml_prefix = "Midi"};
    {in_name = "ext/morph/morph.h"; c_prefix = "LV2_MORPH__"; ml_prefix = "Morph"};
    {in_name = "ext/options/options.h"; c_prefix = "LV2_OPTIONS__"; ml_prefix = "Options"};
    {in_name = "ext/parameters/parameters.h"; c_prefix = "LV2_PARAMETERS__"; ml_prefix = "Parameters"};
    {in_name = "ext/patch/patch.h"; c_prefix = "LV2_PATCH__"; ml_prefix = "Patch"};
    {in_name = "ext/port-groups/port-groups.h"; c_prefix = "LV2_PORT_GROUPS__"; ml_prefix = "PortGroups"};
    {in_name = "ext/port-props/port-props.h"; c_prefix = "LV2_PORT_PROPS__"; ml_prefix = "PortProps"};
    {in_name = "ext/presets/presets.h"; c_prefix = "LV2_PRESETS__"; ml_prefix = "Presets"};
    {in_name = "ext/resize-port/resize-port.h"; c_prefix = "LV2_RESIZE_PORT__"; ml_prefix = "ResizePort"};
    {in_name = "ext/state/state.h"; c_prefix = "LV2_STATE__"; ml_prefix = "State"};
    {in_name = "ext/time/time.h"; c_prefix = "LV2_TIME__"; ml_prefix = "Time"};
    {in_name = "ext/urid/urid.h"; c_prefix = "LV2_URID__"; ml_prefix = "Urid"};
    {in_name = "ext/uri-map/uri-map.h"; c_prefix = "LV2_URI_MAP__"; ml_prefix = "UriMap"};
    {in_name = "ext/worker/worker.h"; c_prefix = "LV2_WORKER__"; ml_prefix = "Worker"};
  ];
