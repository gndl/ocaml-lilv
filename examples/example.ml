open Printf
open Lilv
(*
let rec printPluginClasses ?(idt="") = function
  | [] -> ()
  | pc::tl -> (
      printPluginClass pc idt;
      printPluginClasses tl ~idt)

and printPluginClass pc idt =
  printf "%sPlugin class label %s\n" idt (Lilv.as_string (PluginClass.get_label pc));
  printf "%sPlugin class uri %s\n" idt (Lilv.as_string (PluginClass.get_uri pc));
  printf "%sPlugin class parent uri %s\n" idt (Lilv.as_string (PluginClass.get_parent_uri pc));
  printf "%sChildren :\n" idt;
  printPluginClasses ~idt:(idt^"\t") (PluginClass.get_children pc)

let printUi ui =
  printf "\tUI uri %s\n" (Lilv.as_string (UI.get_uri ui));
  printf "\tUI bundle uri %s\n" (Lilv.as_string (UI.get_bundle_uri ui));
  printf "\tUI binary uri %s\n" (Lilv.as_string (UI.get_binary_uri ui));
  List.iter (fun v -> printf "\t\tclasse %s\n" (Lilv.as_string v)) (UI.get_classes ui)

let printPlugin plg =
  printf "Plugin %s :\n" (Lilv.as_string (Plugin.get_name plg));
  printf "\tclass %s\n" (Lilv.as_string (PluginClass.get_label(Plugin.get_class plg)));
  printf "\turi %s\n" (Lilv.as_string (Plugin.get_uri plg));
  printf "\tbundle uri %s\n" (Lilv.as_string (Plugin.get_bundle_uri plg));
  printf "\tlibrary uri %s\n" (Lilv.as_string (Plugin.get_library_uri plg));
  List.iter (fun v -> printf "\tdata uri %s\n" (Lilv.as_string v)) (Plugin.get_data_uris plg);
  printf "\tauthor name %s\n" (Lilv.as_string (Plugin.get_author_name plg));
  printf "\tauthor email %s\n" (Lilv.as_string (Plugin.get_author_email plg));
  printf "\tauthor homepage %s\n" (Lilv.as_string (Plugin.get_author_homepage plg));
  (*printf "\tverify %s\n" (if Plugin.verify plg then "OK" else "KO");*)

  List.iter (fun v -> printf "\trequired features : %s\n" (Lilv.as_string v)) (Plugin.get_required_features plg);
  List.iter (fun v -> printf "\toptional features : %s\n" (Lilv.as_string v)) (Plugin.get_optional_features plg);

  if Plugin.has_latency plg then
    printf "\thas latency on port %d\n" (Plugin.get_latency_port_index plg);

  printf "\tnum ports %d\n" (Plugin.get_num_ports plg);

  let (mins, maxs, defs) = Plugin.get_port_ranges_float plg in

  for i = 0 to Plugin.get_num_ports plg - 1 do
    let prt = Plugin.get_port_by_index plg i in
    printf "\tport %d : %s (%s)\n" i (Lilv.as_string(Port.get_name plg prt))
      (Lilv.as_string(Port.get_symbol plg prt));

    List.iter (fun v -> printf "\t\thas property %s\n" (Lilv.as_string v)) (Port.get_properties plg prt);

    printf "\t\tmin = %f, max = %f, def = %f\n"
      (Bigarray.Array1.get mins i) (Bigarray.Array1.get maxs i) (Bigarray.Array1.get defs i);

    List.iter (fun sp -> printf "\t\tScalePoint %s : %s\n"
		  (Lilv.as_string(ScalePoint.get_label sp))
		  (Lilv.as_string(ScalePoint.get_value sp))
	      )	(Port.get_scale_points plg prt);
  done;

  List.iter printUi (Plugin.get_uis plg)


let _ =
  let w = World.make() in
  World.load_all w;

  printPluginClasses (World.get_plugin_classes w);

  List.iter printPlugin (World.get_all_plugins w)

*)

let () = printf"Example %s\n" "1"
