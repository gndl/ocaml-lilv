open Printf
open Lilv

let rec print_plugin_class idt pc =
  printf "%s %s %s (%s)\n" idt
    (Pluginclass.get_label pc)
    (Pluginclass.get_uri pc)
    (Pluginclass.get_parent_uri pc);
  Array.iter(print_plugin_class(idt^"\t")) (Pluginclass.get_children pc)

let print_port port =
  printf "\tPort %s (%s)\n\t\tClasses :\n" (Port.get_name port) (Port.get_symbol port);
  Array.iter(printf"\t\t %s\n") (Port.get_classes port);
  printf "\t\tProperties :\n";
  Array.iter(printf"\t\t %s\n") (Port.get_properties port);

  let def_value, min_value, max_value = Port.get_range port in

  printf"\t\tdef_value %f, min_value %f, max_value %f\n" def_value min_value max_value;

  match Port.get port Lv2.ResizePort_minimumSize with
  | Some node when Node.is_int node -> printf "\t\tMinimum size : %d\n" (Node.as_int node)
  | _ -> printf "\t\tNO MINIMUM SIZE\n"


let print_instance instance =
  printf "\tInstance %s\n" (Instance.get_uri instance)

let print_plugin features plugin =
  printf "Plugin %s %s\n" (Plugin.get_name plugin) (Plugin.get_uri plugin);
  print_plugin_class "\tClass" (Plugin.get_class plugin);
  (*
  *)
  let _ = match Plugin.instantiate plugin 44100. features with
    | Ok instance -> print_instance instance
    | Error `Lilv_plugin_instantiation_failure msg -> Printf.eprintf "%s\n%!" msg
  in
  Array.iter print_port (Plugin.get_ports plugin);

  let nb_port = Plugin.get_num_ports(plugin) in
  let min_values, max_values, def_values = Plugin.get_port_ranges_float(plugin) in

  for i = 0 to nb_port - 1 do
    printf"\t\tPort %d min_value %f, max_value %f, def_value %f\n" i min_values.{i} max_values.{i} def_values.{i}
  done

    (*
  printf "\tclass %s\n" (Lilv.as_string (PluginClass.get_label(Plugin.get_class plg)));
  printf "\turi %s\n" (Lilv.as_string (Plugin.get_uri plg));
  printf "\tbundle uri %s\n" (Lilv.as_string (Plugin.get_bundle_uri plg));
  printf "\tlibrary uri %s\n" (Lilv.as_string (Plugin.get_library_uri plg));
  List.iter (fun v -> printf "\tdata uri %s\n" (Lilv.as_string v)) (Plugin.get_data_uris plg);
  printf "\tauthor name %s\n" (Lilv.as_string (Plugin.get_author_name plg));
  printf "\tauthor email %s\n" (Lilv.as_string (Plugin.get_author_email plg));
  printf "\tauthor homepage %s\n" (Lilv.as_string (Plugin.get_author_homepage plg));
  (*printf "\tverify %s\n" (if Plugin.verify plg then "OK" else "KO");*)

  List.iter (fun v -> printf "\trequired features : %s\n" (Lilv.as_string v)) (Plugin.get_required_features plg);
  List.iter (fun v -> printf "\toptional features : %s\n" (Lilv.as_string v)) (Plugin.get_optional_features plg);

  if Plugin.has_latency plg then
    printf "\thas latency on port %d\n" (Plugin.get_latency_port_index plg);

  printf "\tnum ports %d\n" (Plugin.get_num_ports plg);

  let (mins, maxs, defs) = Plugin.get_port_ranges_float plg in

  for i = 0 to Plugin.get_num_ports plg - 1 do
    let prt = Plugin.get_port_by_index plg i in
    printf "\tport %d : %s (%s)\n" i (Lilv.as_string(Port.get_name plg prt))
      (Lilv.as_string(Port.get_symbol plg prt));

    List.iter (fun v -> printf "\t\thas property %s\n" (Lilv.as_string v)) (Port.get_properties plg prt);

    printf "\t\tmin = %f, max = %f, def = %f\n"
      (Bigarray.Array1.get mins i) (Bigarray.Array1.get maxs i) (Bigarray.Array1.get defs i);

    List.iter (fun sp -> printf "\t\tScalePoint %s : %s\n"
(Lilv.as_string(ScalePoint.get_label sp))
(Lilv.as_string(ScalePoint.get_value sp))
)	(Port.get_scale_points plg prt);
  done;

  List.iter printUi (Plugin.get_uis plg)
*)
let add_feature features = function
  | Ok f -> f::features
  | Error `Lilv_feature_failure msg -> Printf.eprintf "%s\n%!" msg; features

let () =
  let _ = match World.load_all() with
    | Ok world ->
      printf "There are %d plugin classes :\n%!" (Array.length (World.get_plugin_classes world));
      print_plugin_class "" (World.get_plugin_class world);
      let plugins = World.get_all_plugins world in
      printf "There are %d plugins!\n%!" (Array.length plugins);

      let features = List.fold_left add_feature [] [
          Feature.lv2 Lv2.State_loadDefaultState;
          Feature.lv2 Lv2.BufSize_powerOf2BlockLength;
          Feature.lv2 Lv2.BufSize_fixedBlockLength;
          Feature.lv2 Lv2.BufSize_boundedBlockLength;
          Feature.log(fun _ m -> print_endline m);
          Feature.default_urid_map();
          Feature.default_urid_unmap();
        ] in

      Array.iter (print_plugin features) plugins
    | Error `Lilv_world_initialization_failure msg -> Printf.eprintf "%s\n%!" msg
    (*  printPluginClasses (World.get_plugin_classes w);  *)
  in
  Gc.full_major (); Gc.full_major ();  (* *)
  printf "Test OK!\n%!";
